<?php

$params = array_merge(
        require __DIR__ . '/../../common/config/params.php',
        require __DIR__ . '/../../common/config/params-local.php',
        require __DIR__ . '/params.php',
        require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'logVars' => ['_GET', '_POST', '_SERVER.REQUEST_URI', '_SESSION'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'google' => [
                    'class' => 'yii\authclient\clients\Google',
                    'clientId' => '887954796143-ga2959oglnjt0ep7lp3pf5j09ef4g8bf.apps.googleusercontent.com',
                    'clientSecret' => '6xnE4CEwrBt9ZUV6RmcO51TM',
                    'name' => 'google',
                    'title' => 'Google Account',
                ],
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'clientId' => '1342528279208648',
                    'clientSecret' => '6c7870130439e3adaa11338bf2a1bdd5',
                ],
                'artivisi' => [
                    'class' => 'yii\authclient\OpenIdConnect',
                    'issuerUrl' => 'https://keycloak.artivisi.id/auth',
                    'clientId' => 'yii2app',
                    'clientSecret' => '74528c43-5f39-4dac-8124-721cd2a15583',
                    'name' => 'artivisi',
                    'title' => 'Artivisi Connect',
                    'validateJws' => false,
                    'tokenUrl' => 'https://keycloak.artivisi.id/auth/realms/belajar/protocol/openid-connect/token',
                    'authUrl' => 'https://keycloak.artivisi.id/auth/realms/belajar/protocol/openid-connect/auth',
                    'apiBaseUrl' => 'https://keycloak.artivisi.id/auth/realms/belajar/protocol/openid-connect'
                ],
            ],
        ]
    /*
      'urlManager' => [
      'enablePrettyUrl' => true,
      'showScriptName' => false,
      'rules' => [
      ],
      ],
     */
    ],
    'params' => $params,
];
