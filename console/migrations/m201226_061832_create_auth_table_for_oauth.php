<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m201226_061832_create_auth_table_for_oauth
 */
class m201226_061832_create_auth_table_for_oauth extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('auth', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'source' => Schema::TYPE_STRING,
            'source_id' => Schema::TYPE_STRING
        ]);

        $this->addForeignKey("auth_fk_user_id", "auth", "user_id", "user", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropTable("auth");
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m201226_061832_create_auth_table_for_oauth cannot be reverted.\n";

      return false;
      }
     */
}
