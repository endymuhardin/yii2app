<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace console\controllers;

use yii\console\Controller;
use Yii;

class HelloController extends Controller {

    public $message;

    public function options($actionID) {
        return ['message'];
    }

    public function optionAliases() {
        return ['m' => 'message'];
    }

    public function actionIndex() {
        $check = true; 
        $total = 0;
        while ($check) {
            $this->message = "ini adalah pesan " . rand(400, 500);
            echo $this->message . "\n";
            Yii::warning($this->message);
            
            usleep(5000);
//            sleep(2);
//            if ($total == 100){
//                $check =  false; 
//            }
//            
            Yii::getLogger()->flush(true);
        }
    }

}
